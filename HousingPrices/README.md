# First Steps with Tensorflow Google Crash Course
This is an exercise in getting familar with tensorflow based on the Google crash course for ML/TensorFlow

Objectives:
* Learn fundimental TensorFlow concepts
* Use linearRegressor class in tensorflow to predict meadian housing proces, at he granularity of city blocks, based on one input feature
* Evaluate the accuracy of a model's predictions using Root Mean Squared Error
* Improve the accuracy of a model by tuning its hyperparameters

The data is based on 1990 census data from California.
https://developers.google.com/machine-learning/crash-course/california-housing-data-description

## Results:
```
Print Mean Squared Error and Root Mean Squared Error.
Mean Squared Error (on training data): 56367.025
Root Mean Squared Error (on training data): 237.417

Let's compare the RMSE to the difference of the min and max of our targets:
Min. Median House Value: 14.999
Max. Median House Value: 500.001
Difference between Min. and Max.: 485.002
Root Mean Squared Error: 237.417

Look at how well our predictions match our targets
Training model...
RMSE (on training data):
  period 00 : 236.32
  period 01 : 235.11
  period 02 : 233.90
  period 03 : 232.70
  period 04 : 231.50
  period 05 : 230.31
  period 06 : 229.13
  period 07 : 227.96
  period 08 : 226.79
  period 09 : 225.63
Model training finished.
       predictions  targets
count      17000.0  17000.0
mean          13.2    207.3
std           10.9    116.0
min            0.0     15.0
25%            7.3    119.4
50%           10.6    180.4
75%           15.8    265.0
max          189.7    500.0
Final RMSE (on training data): 225.63

For RSME for a 180 or below
Training model...
RMSE (on training data):
  period 00 : 225.63
  period 01 : 214.42
  period 02 : 204.04
  period 03 : 194.97
  period 04 : 187.23
  period 05 : 180.53
  period 06 : 175.22
  period 07 : 171.40
  period 08 : 168.39
  period 09 : 166.96
Model training finished.
       predictions  targets
count      17000.0  17000.0
mean         119.5    207.3
std           98.5    116.0
min            0.1     15.0
25%           66.1    119.4
50%           96.1    180.4
75%          142.4    265.0
max         1714.7    500.0
Final RMSE (on training data): 166.96

For Population as the input_feature
Training model...
RMSE (on training data):
  period 00 : 225.63
  period 01 : 214.84
  period 02 : 204.86
  period 03 : 196.10
  period 04 : 189.66
  period 05 : 184.57
  period 06 : 180.67
  period 07 : 178.13
  period 08 : 176.45
  period 09 : 175.94
Model training finished.
       predictions  targets
count      17000.0  17000.0
mean         122.1    207.3
std           98.0    116.0
min            0.3     15.0
25%           67.5    119.4
50%           99.7    180.4
75%          147.0    265.0
max         3047.3    500.0
Final RMSE (on training data): 175.94

```